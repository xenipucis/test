package es.ms.test.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Value("${value1}")
    private String value1;

    @GetMapping("/test")
    public String test() {
        return "hello, World!!; value1 = " + value1;
    }

    private String getValue1() {
        return value1;
    }

    private void setValue1(final String value1) {
        this.value1 = value1;
    }

}

